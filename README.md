Last updated: 2019-02-07 14:32 EST

**Background & Overview**

A few months ago, I set up a new Linux system using Kubuntu 18.04 LTS. I installed Thunderbird 60.4.0 as a package using the "Mozilla Team" PPA (<https://launchpad.net/~mozillateam/+archive/ubuntu/ppa/>). As soon as I started using this version of Thunderbird, I realized that the display format of date/time values was not what I wanted, and that I could not properly control the format. I was extremely annoyed. For me, this is a huge productivity issue, and completely unacceptable. Based on my 35+ years of experience, I consider it a serious bug. It certainly seems user-hostile from a software design standpoint.

I researched the issue on-line, and found that this bug was brought to the attention of Mozilla on 2017-12-22 via their bug tracking system (<https://bugzilla.mozilla.org/show_bug.cgi?id=1426907>). As of now, more than 13 months and 113 comments later, it does not seem that Mozilla has properly acknowledged the bug or taken any serious steps to fix it. Work-arounds are suggested that require changing the platform's time locale and, in some cases, altering the LC_TIME environment variable before running Thunderbird. As far as I am concerned, expecting a user to hack into their platform's locale system to deal with a bug in an application is totally unreasonable, and just plain wrong.

About three weeks ago, I decided to try to fix the bug myself. I've never looked at Mozilla's source code before, so I was not very optimistic. Given the situation, I decided to stop installing Thunderbird using my platform's package system, and build from source instead, sticking with release version 60.4.0. I could not find the source for 60.4.0 clearly identified in Mozilla's repository, so I downloaded it from their FTP site instead.

I had a lot of trouble getting the build to work due to build tool dependencies that are not clearly documented anywhere that I could find. I then had a lot of trouble getting the build options set correctly. Again, I couldn't find any clear documentation anywhere explaining how the release versions from the PPA are built. Eventually, I got the build process working satisfactorily and was able to focus on the actual date/time formatting bug. After evaluating the code, I came up with two solutions to fix this bug, both requiring minimal code modification.

Note that I have only posted the files that required modification in this project. If you want all the source, please get it from Mozilla. There are three branches in this project - "original", "modified", and "modified_with_printf". The last one is intended for debugging or evaluation purposes only.

Note also that I posted a comment to the bug tracking system (<https://bugzilla.mozilla.org/show_bug.cgi?id=1426907#c113>) with a link to this project. I cannot pursue this any further due to time constraints. Perhaps someone else will pick up where I left off, and submit the modifications to Mozilla for consideration.

**Details**

The first solution involves modifications to the file [intl/locale/gtk/OSPreferences_gtk.cpp](https://gitlab.com/jeff_harp/thunderbird_60.4.0_date-time_mods/blob/modified/intl/locale/gtk/OSPreferences_gtk.cpp) to implement a new string preference named _intl.locale.date_\__time.lc_\__time_\__override_. If this preference is found, its value is used instead of the LC_TIME environment variable value. An example value for this preference is "root.UTF-8", which yields the ISO 8601 date/time format users want. This preference eliminates the need for fiddling with the platform's locale system or changing LC_TIME when starting Thunderbird.

The second solution involves modifications to the file [intl/locale/DateTimeFormat.cpp](https://gitlab.com/jeff_harp/thunderbird_60.4.0_date-time_mods/blob/modified/intl/locale/DateTimeFormat.cpp) to implement a new string preference named _intl.locale.date_\__time.pattern_\__override_. If this preference is found, its value is used as the date/time pattern instead of the one that Mozilla comes up with. An example value for this preference is "y-MM-dd hh:mm", which yields the ISO 8601 date/time format users want. This preference provides very direct, granular control over the display of date/time values.

These two solutions are not mutually exclusive. However, strictly speaking, you only need one to fix the bug. Both empower the user, giving them more control over their interface. And both could be useful to developers as well.

Based on my experimentation, the LC_TIME override gets used once at application start, so changes to that preference would require restarting to take effect. The pattern override seems to be used twice whenever a date/time value is displayed, so changes to that preference take effect immediately, as the UI is rendered/refreshed.
